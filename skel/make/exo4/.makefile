CC=gcc
EXEC=hello


all: $(EXEC)

hello: hello.o main.o
	    $(CC) -o $@ $^

hello.o: hello.c
	    $(CC) -o $@ -c $<

main.o: main.c hello.h
	    $(CC) -o $@ -c $<

clean:
	    rm -rf *.o

mrproper: clean
	    rm -rf $(EXEC)
